import "react-native-gesture-handler";
import React from "react";
import {StatusBar} from "react-native";
import {NavigationContainer} from "@react-navigation/native";
import Routes from "./src/routes";
import {AuthProvider} from "./src/hooks/auth";

const App = () => {
  return (
    <NavigationContainer>
      <StatusBar barStyle="light-content" translucent />
      <AuthProvider>
        <Routes />
      </AuthProvider>
    </NavigationContainer>
  );
};

export default App;
