import styled, {css} from "styled-components/native";

export const Container = styled.View`
  padding: 0 12px;
`;

export const Header = styled.Text`
  text-align: center;
  margin: 60px 0 30px;

  color: #3199e4;
  font-family: Roboto;
  font-weight: bold;
`;

export const ContentContainer = styled.View``;

export const MessageContainer = styled.Text`
  width: 100%;
  font-style: normal;
  font-weight: bold;
  font-size: 13px;
  color: #ffffff;
`;

export const Author = styled.Text`
  font-weight: bold;
  font-size: 14px;
  line-height: 16px;
  color: #ffffff;
  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`;

interface MessageData {
  count: number;
}

export const Message = styled.View`
  margin-bottom: 12px;
  padding: 15px;
  border-radius: 15px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);

  ${({count}: MessageData) => {
    switch (count) {
      case 1:
        return css`
          background: #4cd2db;
        `;
      case 2:
        return css`
          background: #3199e4;
        `;
      case 3:
        return css`
          background: #255ae0;
        `;
      case 4:
        return css`
          background: #445d8f;
        `;
      case 5:
        return css`
          background: #4b38c1;
        `;
      default:
        return css``;
    }
  }}
`;

export const Date = styled.Text`
  color: rgba(0, 0, 0, 0.35);
`;

export const MessageHeader = styled.View`
  flex-direction: row;
  margin-bottom: 8px;
  justify-content: space-between;
`;

export const Footer = styled.View`
  justify-content: flex-end;
`;
