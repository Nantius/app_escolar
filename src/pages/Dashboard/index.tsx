/* eslint-disable react/jsx-one-expression-per-line */
import React, {useState} from "react";
import {Text, View, ScrollView} from "react-native";
import Icon from "react-native-vector-icons/Feather";
import Button from "../../components/Button";

import {useAuth, UserData} from "../../hooks/auth";

import {
  Container,
  Header,
  MessageContainer,
  Message,
  ContentContainer,
  Author,
  Footer,
  Date,
  MessageHeader,
} from "./styles";

interface messageData {
  id: string;
  text: string;
  author: string;
  date: string;
  authorType: string;
  file?: string;
}

const Dashboard: React.FC = () => {
  let count = 0;
  const [messages, setMessages] = useState<messageData[]>([
    {
      id: "sidiashda",
      author: "Joana Castro",
      date: "14/06/2020",
      text:
        "Prezados pais, nesta segunda-feira não haverá atividades escolares devido ao feriado. As atividades retornarão na terça-feira.",
      authorType: "Diretora",
    },
    {
      id: "sidiaasdasasdshda",
      author: "Claudio Soares",
      date: "14/06/2020",
      text:
        "Prezados pais, nesta segunda-feira não haverá atividades escolares devido ao feriado. As atividades retornarão na terça-feira.",
      authorType: "Pais",
    },
    {
      id: "sidiaasdasasdshda",
      date: "14/06/2020",
      author: "Claudio Soares",
      text:
        "Prezados pais, nesta segunda-feira não haverá atividades escolares devido ao feriado. As atividades retornarão na terça-feira.",
      authorType: "Pais",
    },
    {
      id: "sidiaasdasasdshda",
      date: "14/06/2020",
      author: "Claudio Soares",
      text:
        "Prezados pais, nesta segunda-feira não haverá atividades escolares devido ao feriado. As atividades retornarão na terça-feira.",
      authorType: "Pais",
    },
    {
      id: "sidiaasdasasdshda",
      date: "14/06/2020",
      author: "Claudio Soares",
      text:
        "Prezados pais, nesta segunda-feira não haverá atividades escolares devido ao feriado. As atividades retornarão na terça-feira.",
      authorType: "Pais",
    },
    {
      id: "sidiaasdasasdshda",
      date: "14/06/2020",
      author: "Claudio Soares",
      text:
        "Prezados pais, nesta segunda-feira não haverá atividades escolares devido ao feriado. As atividades retornarão na terça-feira.",
      authorType: "Pais",
    },
  ]);

  return (
    <Container>
      <Header>Suas mensagens</Header>
      <ScrollView
        contentContainerStyle={{flexGrow: 1, backgroundColor: "#fff"}}
      >
        <ContentContainer>
          {messages.map((message) => {
            if (count === 5) {
              count = 1;
            } else {
              count += 1;
            }

            return (
              <Message count={count} key={message.id}>
                <MessageHeader>
                  <Author>
                    {message.author} - {message.authorType}
                  </Author>
                  <Date>{message.date}</Date>
                </MessageHeader>
                <MessageContainer numberOfLines={2}>
                  {message.text}
                </MessageContainer>
              </Message>
            );
          })}
        </ContentContainer>
      </ScrollView>
      <Footer>
        <Button>+ Nova Mensagem</Button>
      </Footer>
    </Container>
  );
};

export default Dashboard;
