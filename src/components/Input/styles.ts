import styled, {css} from 'styled-components/native';
import Icon from 'react-native-vector-icons/Feather';

interface ContainerProps {
  isFocused: boolean;
  hasError: boolean;
}

export const Container = styled.View<ContainerProps>`
  width: 100%;
  height: 60px;
  padding: 0 16px;
  background: white;
  border-radius: 10px;
  margin-bottom: 8px;
  border: 2px;
  border-color: #108EBD;


  flex-direction: row;
  align-items: center;

  ${(props) =>
    props.hasError &&
    css`
      border-color: #c53030;
    `}

  ${(props) =>
    props.isFocused &&
    css`
      border-color: #ff9000;
    `}
`;

export const TextInput = styled.TextInput`
  flex: 1;
  color: black;
  font-size: 16px;
  font-family: 'RobotoSlab-Regular';
`;

export const FeatherIcon = styled(Icon)`
  margin-right: 16px;
`;
