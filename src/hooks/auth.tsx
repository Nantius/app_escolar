import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from "react";
import AsyncStorage from "@react-native-community/async-storage";
// import api from '../services/api';

interface AuthState {
  // token: string;
  user: UserData;
}

interface SignInCredentials {
  email: string;
  password: string;
}

interface AuthContextData {
  user: UserData;
  signIn(credentials: SignInCredentials): Promise<void>;
  signOut(): void;
  loading: boolean;
}

export interface UserData {
  password: string;
  email: string;
  name: string;
  child: string;
  phone: string;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

const AuthProvider: React.FC = ({children}) => {
  const [data, setData] = useState<AuthState>({} as AuthState);
  const [loading, setLoading] = useState(true);
  const [userList, setUserList] = useState<UserData[]>([
    {
      password: "123456",
      email: "spdleonardo@hotmail.com",
      name: "leonardo",
      child: "joão",
      phone: "123456",
    },
  ]);

  useEffect(() => {
    async function loadStoragedData(): Promise<void> {
      const [token, user] = await AsyncStorage.multiGet([
        // '@GoBarber:token',
        "@GoBarber:user",
      ]);

      if (token[1] && user[1]) {
        // setData({token: token[1], user: JSON.parse(user[1])});
        setData({user: JSON.parse(user[1])});
      }
      setLoading(false);
    }
    setLoading(false);
    // loadStoragedData();
  }, []);

  const signIn = useCallback(async ({email, password}) => {
    console.log(email);
    // const response = await api.post('sessions', {
    //   email,
    //   password,
    // });

    const user = userList.filter(
      (u) => u.email === email && u.password === password,
    );
    console.log(user);

    if (!user) {
      throw new Error("Erro na autenticação");
    }

    // const {token, user} = response.data;

    await AsyncStorage.multiSet([
      // ['@GoBarber:token', token],
      ["@GoBarber:user", JSON.stringify(user)],
    ]);
    // setData({token, user});
    setData({user: user[0]});
  }, []);

  const signOut = useCallback(async () => {
    // await AsyncStorage.multiRemove(['@GoBarber:token', '@GoBarber:user']);
    setData({} as AuthState);
  }, []);

  return (
    <AuthContext.Provider value={{user: data.user, signIn, signOut, loading}}>
      {children}
    </AuthContext.Provider>
  );
};

function useAuth(): AuthContextData {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error("useAuth must be used within an AuthProvider");
  }

  return context;
}

export {AuthProvider, useAuth};
